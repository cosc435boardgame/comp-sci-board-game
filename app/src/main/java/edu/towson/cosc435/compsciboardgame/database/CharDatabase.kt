package edu.towson.cosc435.compsciboardgame.database

import androidx.room.*
import edu.towson.cosc435.compsciboardgame.model.Chars
import java.util.*

// Interface for the database, uses ROOM tags
@Dao
interface CharDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addChar(char: Chars)

    @Update
    suspend fun updateChar(char: Chars)

    @Delete
    suspend fun deleteChar(char: Chars)

    @Query("select * from Chars")
    suspend fun getAllChars(): List<Chars>
}

// Type converter that's necessary for converting between UUID and string
class UUIDConverter {
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }
}

// The database itself
@Database(entities = arrayOf(Chars::class), version = 1, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class CharDatabase : RoomDatabase() {
    abstract fun charDao(): CharDao
}