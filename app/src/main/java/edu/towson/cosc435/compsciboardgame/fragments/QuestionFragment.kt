package edu.towson.cosc435.compsciboardgame.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import java.lang.ClassCastException

class QuestionFragment : DialogFragment() {

    // Where the questions and answers are stored, questions and answers that go together have the same index
    val questions = arrayOf("What is an object?",
        "What is a constructor?",
        "What is polymorphism?",
        "What is inheritance?",
        "Is the string class final?",
        "What is a wrapper class?",
        "What is a stream?",
        "What is an interface?",
        "What is an abstract class?",
        "What is a data type?",
        "What is a variable declaration?",
        "What are primitive data types?",
        "What is a file?",
        "What was the first high level programming language?",
        "What is a block?",
        "What is an int?",
        "What are parameters?",
        "What is a class?",
        "What is an array?")

    val answers = arrayOf(  "An instance of a class",
        "A method used to create an object of a class",
        "The ability of an object to take on multiple forms",
        "Passing properties of a parent class onto a child class",
        "Yes",
        "Class that wraps a primitive data type",
        "A sequence of data",
        "An abstract type used to specify behavior classes must implement",
        "A class that contains the abstract keyword in declaration",
        "An attribute of data which tells the compiler how the programmer intends to use the data",
        "The process of defining the type for the variable, and/or being initialized",
        "Predefined types of data which are supported by the programming language",
        "An object on a computer that stores data",
        "Fortran",
        "A programming component that may consist of several statements to be executed in sequence",
        "A data type use to define numeric values",
        "A value that is passed into a function",
        "An extensible program-code template for creating objects",
        "A container that holds a fixed number of similar data types")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Chooses a random question to ask, stores the index for it
        val randomQuestion = (0 until questions.size).random()

        // Chooses which random index in the alertdialog will hold the correct answer
        val correctAnswer = (0..3).random()

        // Creates a null array of strings for the answers that will be displayed so that it can be initialized randomly afterwards
        var items = arrayOfNulls<String>(4)

        // A randomly decided index in the displayed answers is set of the correct answer to the asked question
        items[correctAnswer] = answers[randomQuestion]

        // Iterates through the displayed answers array to set the other indexes as other incorrect answers
        for (x in 0..3) {
            // Skips over the index with the correct answer
            if (x != correctAnswer) {
                // Gets a random answer
                var otherAnswer = (0 until answers.size).random()
                // Checks that the random answer isn't the correct answer
                while (otherAnswer == randomQuestion)
                    otherAnswer = (0 until answers.size).random()
                // Sets the array at the index as the random incorrect answer
                items[x] = answers[otherAnswer]
            }
        }
        Log.d(TAG, items[0] + ". " + items[1] + ". " + items[2] + ". " + items[3] + ".")

        // returns a built alertdialog when QuestionFragment() is called in GameActivity
        return activity?.let {
            val builder = AlertDialog.Builder(it) // Declares builder
            builder
                .setTitle(questions[randomQuestion]) // Sets the title as the question being asked
                .setItems(items, DialogInterface.OnClickListener { _, which -> // Listener for an item being clicked
                listener.onDialogItemClick(which == correctAnswer) // returns true if the selected item is the correct answer
            })
                .setCancelable(false) // Makes the alertdialog unable to be cancelled
                .create() // Creates the builder so it's ready to be shown
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private lateinit var listener: QuestionDialogListener

    // Interface with onDialogItemCick function that gets implemented in GameActivity
    interface QuestionDialogListener {
        fun onDialogItemClick(boolean: Boolean)
    }

    // Necessary for the onDialogItemClick listener function
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as QuestionDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException((context.toString() + " must implement QuestionDialogListener"))
        }
    }

    companion object {
        val TAG = QuestionFragment::class.java.simpleName
    }
}