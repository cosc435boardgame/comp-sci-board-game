package edu.towson.cosc435.compsciboardgame

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.char_select.*
import com.google.gson.Gson
import edu.towson.cosc435.compsciboardgame.database.CharDatabaseRepository
import edu.towson.cosc435.compsciboardgame.interfaces.ICharController
import edu.towson.cosc435.compsciboardgame.interfaces.ICharRepository
import edu.towson.cosc435.compsciboardgame.model.Chars
import java.util.*

class CharacterSelect : AppCompatActivity(), ICharController {

    // Creates the variable for the repository that pulls from the database
    override lateinit var chars: ICharRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.char_select)

        // Initalizes the repository
        chars = CharDatabaseRepository(this)

        // Sets up recyclerview
        recyclerViewchar.layoutManager = LinearLayoutManager(this)
        recyclerViewchar.adapter= CharacterAdapter(this)
        Log.d("Test", "In charSelect")
    }

    // Handles character selection by sending the selected character back to the main activity in an intent result
    override fun handleSelect(idx: Int) {
        val selectedChar = chars.getChar(idx)
        Log.d("TEST", selectedChar.toString())
        val intent = Intent()
        intent.putExtra(CHAR_SEL_MSG_KEY, Gson().toJson(selectedChar))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    companion object{
        const val CHAR_SEL_MSG_KEY = "CHARACTER"
    }
}