package edu.towson.cosc435.compsciboardgame.model

import android.graphics.drawable.Drawable
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Chars (
    @PrimaryKey
    val id: UUID,
    val name: String,
    val image: Int, // Resource Id's for images are int
    val description: String
)