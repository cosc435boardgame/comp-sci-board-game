package edu.towson.cosc435.compsciboardgame.interfaces

// Interface for the controller, all it needs to do is handle the selected charactaer and declare a repository
interface ICharController {
    fun handleSelect(idx: Int)
    val chars: ICharRepository
}