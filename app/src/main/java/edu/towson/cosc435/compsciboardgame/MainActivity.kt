package edu.towson.cosc435.compsciboardgame

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.room.Room
import com.google.gson.Gson
import edu.towson.cosc435.compsciboardgame.model.Chars
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Keeps track of coins collected from the game, will keep increasing from playing multiple times
    private var totalCoins = 0

    // Values to keep track of the resource Id of the selected character, used for passing it to the game activity
    private var selectedChar = R.drawable.adalovelace

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Listener for character selection screen
        sel_char_main_btn.setOnClickListener { launchCharSelect() }

        // Listeners for the game launch buttons
        gameMap1btn.setOnClickListener { launchGrass() }
        gameMap2btn.setOnClickListener { launchLava() }
        gameMap3btn.setOnClickListener { launchIce() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CHAR_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        // Gets the selected character from the intent
                        val character = Gson().fromJson(
                            data?.getStringExtra(CharacterSelect.CHAR_SEL_MSG_KEY),
                            Chars::class.java
                        )
                        Log.d(TAG, character.toString())
                        // Displays the info of the selected character in the main activity
                        mainCharNametxt.text = character.name
                        mainCharDesctxt.text = character.description
                        mainCharImg.setImageResource(character.image)
                        selectedChar = character.image
                    }

                }
            }
            GAME_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        // Gets the number of coins earned from the finished game through the intent
                        val coinsReturned: Int? = data?.getIntExtra("Coins", 0)
                        Log.d(TAG, "Coins returned: $coinsReturned")
                        // Checks that the coins from the intent wasn't null
                        if (coinsReturned != null) {
                            // Adds returned coins to the total coins
                            totalCoins += coinsReturned
                        }
                        // Updates display of the coins
                        textView.text = "$totalCoins"
                    }
                }
            }
        }
    }

    // Launches the character selection screen
    private fun launchCharSelect(){
        val intent = Intent(this, CharacterSelect::class.java)
        startActivityForResult(intent, CHAR_REQUEST_CODE)
    }

    // Launches the game with a grass background, sends selected character in intent
    private fun launchGrass() {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("Character", selectedChar)
        intent.putExtra("Background", R.drawable.grasspattern)
        startActivityForResult(intent, GAME_REQUEST_CODE)
    }

    // Launches the game with a lava background, sends selected character in intent
    private fun launchLava() {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("Character", selectedChar)
        intent.putExtra("Background", R.drawable.lavapattern)
        startActivityForResult(intent, GAME_REQUEST_CODE)
    }

    // Launches the game with an ice background, sends selected character in intent
    private fun launchIce() {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("Character", selectedChar)
        intent.putExtra("Background", R.drawable.icepattern)
        startActivityForResult(intent, GAME_REQUEST_CODE)
    }

    companion object{
        val TAG = MainActivity::class.java.simpleName
        val CHAR_REQUEST_CODE = 1
        val GAME_REQUEST_CODE = 2
    }
}