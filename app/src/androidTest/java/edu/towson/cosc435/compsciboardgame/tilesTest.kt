package edu.towson.cosc435.compsciboardgame


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class tilesTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun tilesTest() {
        val appCompatButton = onView(
            allOf(
                withId(R.id.gameMap1btn), withText("Map 1: Grass"),
                childAtPosition(
                    childAtPosition(
                        withClassName(`is`("android.widget.LinearLayout")),
                        7
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        val imageView = onView(
            allOf(
                withId(R.id.tile1),
                isDisplayed()
            )
        )
        imageView.check(matches(isDisplayed()))

        val imageView2 = onView(
            allOf(
                withId(R.id.tile2),
                isDisplayed()
            )
        )
        imageView2.check(matches(isDisplayed()))

        val imageView3 = onView(
            allOf(
                withId(R.id.tile3),
                isDisplayed()
            )
        )
        imageView3.check(matches(isDisplayed()))

        val imageView4 = onView(
            allOf(
                withId(R.id.tile4),
                isDisplayed()
            )
        )
        imageView4.check(matches(isDisplayed()))

        val imageView5 = onView(
            allOf(
                withId(R.id.tile5),
                isDisplayed()
            )
        )
        imageView5.check(matches(isDisplayed()))

        val imageView6 = onView(
            allOf(
                withId(R.id.tile6),
                isDisplayed()
            )
        )
        imageView6.check(matches(isDisplayed()))

        val imageView7 = onView(
            allOf(
                withId(R.id.tile7),
                isDisplayed()
            )
        )
        imageView7.check(matches(isDisplayed()))

        val imageView8 = onView(
            allOf(
                withId(R.id.tile8),
                isDisplayed()
            )
        )
        imageView8.check(matches(isDisplayed()))

        val imageView9 = onView(
            allOf(
                withId(R.id.tile9),
                isDisplayed()
            )
        )
        imageView9.check(matches(isDisplayed()))

        val imageView10 = onView(
            allOf(
                withId(R.id.tile10),
                isDisplayed()
            )
        )
        imageView10.check(matches(isDisplayed()))

        val imageView11 = onView(
            allOf(
                withId(R.id.tile11),
                isDisplayed()
            )
        )
        imageView11.check(matches(isDisplayed()))

        val imageView12 = onView(
            allOf(
                withId(R.id.tile12),
                isDisplayed()
            )
        )
        imageView12.check(matches(isDisplayed()))

        val imageView13 = onView(
            allOf(
                withId(R.id.tile13),
                isDisplayed()
            )
        )
        imageView13.check(matches(isDisplayed()))

        val imageView14 = onView(
            allOf(
                withId(R.id.tile14),
                isDisplayed()
            )
        )
        imageView14.check(matches(isDisplayed()))

        val imageView15 = onView(
            allOf(
                withId(R.id.tile15),
                isDisplayed()
            )
        )
        imageView15.check(matches(isDisplayed()))

        val imageView16 = onView(
            allOf(
                withId(R.id.tile16),
                isDisplayed()
            )
        )
        imageView16.check(matches(isDisplayed()))

        val imageView17 = onView(
            allOf(
                withId(R.id.tile17),
                isDisplayed()
            )
        )
        imageView17.check(matches(isDisplayed()))

        val imageView18 = onView(
            allOf(
                withId(R.id.tile18),
                isDisplayed()
            )
        )
        imageView18.check(matches(isDisplayed()))

        val imageView19 = onView(
            allOf(
                withId(R.id.tile19),
                isDisplayed()
            )
        )
        imageView19.check(matches(isDisplayed()))

        val imageView20 = onView(
            allOf(
                withId(R.id.tile20),
                isDisplayed()
            )
        )
        imageView20.check(matches(isDisplayed()))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
